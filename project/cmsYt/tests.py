from django.test import Client, SimpleTestCase, TestCase
from . import views
import urllib.request
from . import ytparser
from .models import Video

# Create your tests here.

xmlAux = ""
class TestParser(TestCase):
    def setUp(self):
        """Ejecutando antes de los test"""
        global xmlAux
        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg'
        xmlAux = urllib.request.urlopen(url)

    def test_parser(self):
        """Test del parser"""
        ytparser.Parser.parse(xmlAux)
        self.assertInHTML("<h1>Title: </h1>", ytparser.videos)

    def test_changeSel(self):
        """Test de la función auxiliar changeSel"""
        booleanAux = views.changeSel(True)
        self.assertEqual(booleanAux, False)


class TestViews(TestCase):
    def setUp(self):
        """Ejecutando antes de los test"""
        self.idSeleccionado = 'SPLwYv62-og'
        self.idSeleccionable = 'j3FwKACOARQ'
        Video.objects.create(titulo='JSON práctico', url='https://www.youtube.com/watch?v=SPLwYv62-og',
            videoId='SPLwYv62-og', imagen='https://i4.ytimg.com/vi/SPLwYv62-og/hqdefault.jpg',
            canal='CursosWeb', link='https://www.youtube.com/channel/UC300utwSVAYOoRLEqmsprfg',
            fecha='2020-04-18T12:34:11+00:00', descripcion='Leer documentos JSON desde Python, para poder manejarlos como variables dentro de nuestro programa (listas, diccionarios...), es muy fácil. Te lo explicamos en este ejemplo.',
            seleccionado=True)
        Video.objects.create(titulo='Frikiminutos: Servidor web en producción', url='https://www.youtube.com/watch?v=j3FwKACOARQ',
            videoId='j3FwKACOARQ', imagen='https://i3.ytimg.com/vi/j3FwKACOARQ/hqdefault.jpg',
            canal='CursosWeb', link='https://www.youtube.com/channel/UC300utwSVAYOoRLEqmsprfg',
            fecha='2020-04-16T18:27:09+00:00', descripcion='Notas sobre cómo poner un servidor web, por ejemplo uno creado con Django, en producción: qué sistemas intervienen, qué cambios hay que hacer...',
            seleccionado=False)

    def test_index_status(self):
        """Test de la función index, estado"""
        response = self.client.get('/cmsYt/')
        self.assertEqual(response.status_code, 200)

    def test_index_func(self):
        """Test de la función index, función"""
        response = self.client.get('/cmsYt/')
        self.assertEqual(response.resolver_match.func, views.index)

    def test_index_html(self):
        """Test de la respuesta html de index"""
        response = self.client.get('/cmsYt/')
        self.assertInHTML("<font size=5 color='black'><strong>Listado de vídeos del canal</strong></font>",
            response.content.decode(encoding='UTF-8'))


    def test_id_post_check_status(self):
        """Test de la función id post, el estado debe ser 301 porque después de cambiar
        el vídeo de seleccioando a no seleccionado, se redirige a la url de la app"""
        response = self.client.post('/cmsYt/' + self.idSeleccionado)
        self.assertEqual(response.status_code, 301)

    def test_id_post_check_func(self):
        """Test de la función id get, función"""
        response = self.client.post('/cmsYt/' + self.idSeleccionable)
        self.assertEqual(response.resolver_match.func, views.id)


    def test_id_get_seleccionado_check_status(self):
        """Test de la función id get con video seleccionado, el estado debe ser
        200 porque el vídeo está seleccionado y se puede mostrar su pág de info"""
        response = self.client.get('/cmsYt/' + self.idSeleccionado)
        self.assertEqual(response.status_code, 200)

    def test_id_get_seleccionado_check_func(self):
        """Test de la función id get con video seleccionado, función"""
        response = self.client.get('/cmsYt/' + self.idSeleccionado)
        self.assertEqual(response.resolver_match.func, views.id)

    def test_id_get_seleccionado_check_html(self):
        """Test de la respuesta html de get seleccionado"""
        response = self.client.get('/cmsYt/' + self.idSeleccionado)
        self.assertInHTML("<font size=4 color='black'><strong><h1>Información del vídeo seleccionado</h1></strong></font>",
            response.content.decode(encoding='UTF-8'))


    def test_id_get_seleccionable_check_status(self):
        """Test de la función id get con video seleccionable, el estado debe ser
        404 porque el vídeo no está seleccionado y no se puede mostrar su info"""
        response = self.client.get('/cmsYt/' + self.idSeleccionable)
        self.assertEqual(response.status_code, 404)

    def test_id_get_seleccionable_check_func(self):
        """Test de la función id get con video seleccionable, función"""
        response = self.client.get('/cmsYt/' + self.idSeleccionable)
        self.assertEqual(response.resolver_match.func, views.id)

    def test_id_get_seleccionable_check_html(self):
        """Test de la respuesta html de get seleccionable"""
        response = self.client.get('/cmsYt/' + self.idSeleccionable)
        self.assertInHTML("<h1>Este vídeo no existe o no ha sido previamente seleccionado.</h1>",
            response.content.decode(encoding='UTF-8'))
