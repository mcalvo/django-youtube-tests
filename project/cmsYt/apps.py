from django.apps import AppConfig
import urllib.request
import sys

xmlStream = ""

class CmsYtConfig(AppConfig):
    name = 'cmsYt'

    def ready(self):
        global xmlStream
        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg'
        xmlStream = urllib.request.urlopen(url)
