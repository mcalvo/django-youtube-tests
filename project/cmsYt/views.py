from django.shortcuts import render, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from .models import Video
from .apps import xmlStream
import urllib.parse


videos = ""
class YTHandler(ContentHandler):

    def __init__ (self):
        self.inEntry = False
        self.inContent = False
        self.content = ""
        self.title = ""
        self.link = ""
        self.channel = ""
        self.channelLink = ""
        self.date = ""
        self.description = ""
        self.image = ""
        self.videoId = ""

    def startElement (self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title' or name == 'yt:videoId' or name == 'name' or name == 'uri' \
                or name == 'published' or name == 'media:description':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')
            elif name == 'media:thumbnail':
                self.image = attrs.get('url')

    def endElement (self, name):
        global videos
        if name == 'entry':
            self.inEntry = False
            videos = videos \
                     + "<h1>Title: </h1><li><a href='" + self.link + "'>" \
                     + self.title + "</a></li>\n"
            try: # comprueba que el video no esté ya en la base de datos
                v = Video.objects.get(titulo=self.title)
            except Video.DoesNotExist: # si el vídeo no está en la base de datos, lo añade
                v = Video(titulo=self.title, url=self.link, imagen=self.image,
                    canal=self.channel, link=self.channelLink, fecha=self.date,
                    descripcion=self.description, seleccionado=False, videoId=self.videoId)
                v.save()
        elif self.inEntry:
            if name == 'title':
                self.title = self.content
            elif name == 'yt:videoId':
                self.videoId = self.content
            elif name == 'name':
                self.channel = self.content
            elif name == 'uri':
                self.channelLink = self.content
            elif name == 'published':
                self.date = self.content
            elif name == 'media:description':
                self.description = self.content

            self.content = ""
            self.inContent = False#

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars


def parse(Parser):
    if Parser == "": # en caso de que la base de datos esté vacía o de que
    # se vuelva a lanzar la app, se actualiza la base de datos
        Parser = make_parser()
        Parser.setContentHandler(YTHandler())
        Parser.parse(xmlStream)
        print(xmlStream)
        return Parser

def videoInfo(selected, video, request):
    if selected: # solo debe devolver la página si el vídeo estaba seleccionado
        context ={'video': video}
        template = 'cmsYt/content.html'
        status = 200
    else: # si el vídeo no está seleccionado, devuelve una página de error
        context = {}
        template = 'cmsYt/error.html'
        status = 404
    return context, template, status

def changeSel(selected):
    return not selected

# Create your views here.

Parser = ""

@csrf_exempt
def index(request):
    global Parser
    if request.method == 'GET': # cuando se hace GET sobre el recurso /
        Parser = parse(Parser)
        videos = Video.objects.all() # obtiene todos los objetos del modelo
        context = {'videos': videos}
        respuesta = render(request, 'cmsYt/index.html', context, status=200)
        # index = index.format(videos=videos)
        return respuesta

@csrf_exempt
def id(request, id): # si se llama con el método POST, cambia un vídeo de seleccionado
# a eliminado y viceversa, si es con GET, devuelve la información principal de dicho vídeo
    try:
        v = Video.objects.get(videoId=id)
        selected = v.seleccionado
        if request.method == 'POST':
            v.seleccionado = changeSel(selected)
            v.save()
            context = {}
            return render(request, 'cmsYt/redirect.html', context, status=301)
        elif request.method == 'GET':
            # if v.seleccionado: # solo debe devolver la página si el vídeo estaba seleccionado
            #     context ={'video': v}
            #     return render(request, 'cmsYt/content.html', context)
            # else: # si el vídeo no está seleccionado, devuelve una página de error
            #     context = {}
            #     return render(request, 'cmsYt/error.html', context, status=404)
            context, template, videoStatus = videoInfo(selected, v, request)
            return render(request, template, context, status=videoStatus)
    except Video.DoesNotExist: # cuando el vídeo no existe, devuelve un error
        context = {}
        return render(request, 'cmsYt/error.html', context, status=404)
