from django.db import models
# from django.utils import timezone

# Create your models here.

class Video(models.Model):
    titulo = models.CharField(max_length=200)
    url = models.CharField(max_length=200)
    videoId = models.CharField(max_length=200, default='')
    imagen = models.TextField(blank=False)
    canal = models.CharField(max_length=200)
    link = models.CharField(max_length=200)
    fecha = models.TextField(blank=False, default='')
    descripcion = models.TextField(blank=False)
    seleccionado = models.BooleanField()
    def __str__(self):
        return self.titulo
